//
//  FlickrPhotoCell.swift
//  FlickrSearchApp
//
//  Created by Dane Wikstrom on 10/20/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit

class FlickrPhotoCell: UICollectionViewCell
{
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
    

}
