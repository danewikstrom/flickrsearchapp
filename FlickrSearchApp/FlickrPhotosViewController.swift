//
//  FlickrPhotosViewController.swift
//  FlickrSearchApp
//
//  Created by Dane Wikstrom on 10/20/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit

final class FlickrPhotosViewController: UICollectionViewController
{
    
    fileprivate let reuseIdentifier = "FlickrCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 10.0, bottom: 30.0, right: 10.0)
    /*each time you perform a search, it displays a new section in the collection view with the results.
    data structure so you can keep the data for each section separate*/
    fileprivate var searches = [FlickrSearchResults]()
    fileprivate let flickr = Flickr()//reference to the object that will do the searching for you
    fileprivate let itemsPerRow: CGFloat = 2
    
    /*
     Poperty to keep track of tapped cells
     
     1. largePhotoIndexPath is an optional that will hold the index path of the tapped photo, if there is one.
     2. Whenever this property gets updated, the collection view needs to be updated. a didSet property observer is the safest place to manage this. There may be two cells that need reloading, if the user has tapped one cell then another, or just one if the user has tapped the first cell, then tapped it again to shrink.
     3. performBatchUpdates will animate any changes to the collection view performed inside the block. You want it to reload the affected cells.
     4. Once the animated update has finished, it’s a nice touch to scroll the enlarged cell to the middle of the screen
     */
    //1
    var largePhotoIndexPath: IndexPath?
    {
        didSet
        {
            //2
            var indexPaths = [IndexPath]()
            if let largePhotoIndexPath = largePhotoIndexPath
            {
                indexPaths.append(largePhotoIndexPath)
            }
            if let oldValue = oldValue
            {
                indexPaths.append(oldValue)
            }
            //3
            collectionView?.performBatchUpdates({
                self.collectionView?.reloadItems(at: indexPaths)
            }) { completed in
                //4
                if let largePhotoIndexPath = self.largePhotoIndexPath
                {
                    self.collectionView?.scrollToItem(
                        at: largePhotoIndexPath,
                        at: .centeredVertically,
                        animated: true)
                }
            }
        }
    }
}


//method that will get a specific photo related to an index path in your collection view
private extension FlickrPhotosViewController
{
    func photoForIndexPath(_ indexPath: IndexPath) -> FlickrPhoto
    {
        return searches[(indexPath as NSIndexPath).section].searchResults[(indexPath as NSIndexPath).row]
    }
}

/*
 1. After adding an activity view, use the Flickr wrapper class I provided to search Flickr for photos that match the given search term asynchronously. When the search completes, the completion block will be called with a the result set of FlickrPhoto objects, and an error (if there was one).
 2. Log any errors to the console. Obviously, in a production application you would want to display these errors to the user.
 3. The results get logged and added to the front of the searches array
 4. At this stage, you have new data and need to refresh the UI. You’re using the reloadData() method, which works just like it does in a table view.
*/
extension FlickrPhotosViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // 1
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        textField.addSubview(activityIndicator)
        activityIndicator.frame = textField.bounds
        activityIndicator.startAnimating()
        
        flickr.searchFlickrForTerm(textField.text!)
        {
            results, error in

            activityIndicator.removeFromSuperview()

            if let error = error
            {
                // 2
                print("Error searching : \(error)")
                return
            }
            
            if let results = results
            {
                // 3
                print("Found \(results.searchResults.count) matching \(results.searchTerm)")
                self.searches.insert(results, at: 0)
                
                // 4
                self.collectionView?.reloadData()
            }
        }
        textField.text = nil
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - UICollectionViewDataSource

//returns information about the number of items in the collection view and their views.

/*
1. There’s one search per section, so the number of sections is the count of the searches array.
2. The number of items in a section is the count of the searchResults array from the relevant FlickrSearch object.
3.

 a. Always stop the activity spinner – you could be reusing a cell that was previously loading an image
 b. This part is as before – if you’re not looking at the large photo, just set the thumbnail and return
 c. If the large image is already loaded, set it and return
 d. By this point, you want the large image, but it doesn’t exist yet. Set the thumbnail image and start the spinner going. The thumbnail will stretch until the download is complete
 e. Ask for the large image from Flickr. This loads the image asynchronously and has a completion block
 f. The load has finished, so stop the spinner
 g. If there was an error or no photo was loaded, there’s not much you can do.
 h. Check that the large photo index path hasn’t changed while the download was happening, and retrieve whatever cell is currently in use for that index path (it may not be the original cell, since scrolling could have happened) and set the large image.

*/
extension FlickrPhotosViewController
{
    //1
    override func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return searches.count
    }
    //2
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int
    {
        return searches[section].searchResults.count
    }
    //3
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: reuseIdentifier, for: indexPath) as! FlickrPhotoCell
        let flickrPhoto = photoForIndexPath(indexPath)
        //a
        cell.activityIndicator.stopAnimating()
        //b
        guard indexPath == largePhotoIndexPath else
        {
            cell.imageView.image = flickrPhoto.thumbnail
            cell.titleLabel.text = flickrPhoto.title
            return cell
        }
        //c
        guard flickrPhoto.largeImage == nil else
        {
            cell.imageView.image = flickrPhoto.largeImage
            cell.titleLabel.text = flickrPhoto.title
            return cell
        }
        //d
        cell.imageView.image = flickrPhoto.thumbnail
        cell.titleLabel.text = flickrPhoto.title
        cell.activityIndicator.startAnimating()
        //e
        flickrPhoto.loadLargeImage { loadedFlickrPhoto, error in
            //f
            cell.activityIndicator.stopAnimating()
            //g
            guard loadedFlickrPhoto.largeImage != nil && error == nil else
            {
                return
            }
            //h
            if let cell = collectionView.cellForItem(at: indexPath) as? FlickrPhotoCell,
                indexPath == self.largePhotoIndexPath
            {
                cell.imageView.image = loadedFlickrPhoto.largeImage
                cell.titleLabel.text = flickrPhoto.title
            }
        }
        return cell
    }
}


// MARK: - UICollectionViewDelegate
/*
 If the tapped cell is already the large photo, set the largePhotoIndexPath property to nil, otherwise set it to the index path the user just tapped. This will then call the property observer you added earlier and cause the collection view to reload the affected cell(s).
*/
extension FlickrPhotosViewController
{
    override func collectionView(_ collectionView: UICollectionView,
                                 shouldSelectItemAt indexPath: IndexPath) -> Bool
    {
        largePhotoIndexPath = largePhotoIndexPath == indexPath ? nil : indexPath
        return false
    }
}

//allow the view controller to conform to the flow layout delegate protocol
//Setting this size of the cell in storyboard doesn’t affect the cells in the app, because I've implemented the delegate method to give a size for each cell, which overwrites anything set in the storyboard
/*
 
 1. collectionView(_:layout:sizeForItemAt:) is responsible for telling the layout the size of a given cell
 2. Here, you work out the total amount of space taken up by padding. There will be n + 1 evenly sized spaces, where n is the number of items in the row. The space size can be taken from the left section inset. Subtracting this from the view’s width and dividing by the number of items in a row gives you the width for each item. You then return the size as a square
 3. collectionView(_:layout:insetForSectionAt:) returns the spacing between the cells, headers, and footers. A constant is used to store the value.
 4. This method controls the spacing between each line in the layout. You want this to match the padding at the left and right.
 */
extension FlickrPhotosViewController : UICollectionViewDelegateFlowLayout
{
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //calculates the size of the cell to fill as much of the collection view as possible whilst maintaining its aspect ratio.
        if indexPath == largePhotoIndexPath
        {
            let flickrPhoto = photoForIndexPath(indexPath)
            var size = collectionView.bounds.size
            size.height -= (sectionInsets.top + sectionInsets.right)
            size.width -= (sectionInsets.left + sectionInsets.right)
            return flickrPhoto.sizeToFillWidthOfSize(size)
        }
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return sectionInsets
    }
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return sectionInsets.left
    }
}
